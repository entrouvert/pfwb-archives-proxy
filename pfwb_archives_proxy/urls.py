"""pfwb_archives_proxy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, register_converter

from . import settings, views


class DocumentTypeConverter:
    regex = '(%s)' % '|'.join(settings.TABELLIO_DOCUMENT_TYPES)
    to_python = lambda self, x: x
    to_url = lambda self, x: x


class DateConverter:
    regex = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    to_python = lambda self, x: x
    to_url = lambda self, x: x


register_converter(DocumentTypeConverter, 'doctype')
register_converter(DateConverter, 'date')

urlpatterns = [
    path('documents/<str:sess>/<doctype:doctype>/<date:date>/', views.redirect),
    path('documents/<str:sess>/<doctype:doctype>/<str:no>/<int:nodoc>/annexe-<str:anx>/', views.redirect),
    path('documents/<str:sess>/<doctype:doctype>/<str:no>/<int:nodoc>/', views.redirect),
    path('documents/<str:sess>/<doctype:doctype>/<str:no>/', views.redirect),
    path('documents/<str:sess>/<str:no>/<int:nodoc>/annexe-<str:anx>/', views.redirect),
    path('documents/<str:sess>/<str:no>/<int:nodoc>/', views.redirect),
    path('documents/<str:sess>/<str:no>/annexe-<str:anx>/', views.redirect),
    path('documents/<str:sess>/<str:no>/', views.redirect),
]
