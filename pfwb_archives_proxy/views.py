import psycopg2

from django.conf import settings
from django.http import HttpResponseRedirect, Http404


def redirect(request, sess, no=None, nodoc=None, anx=None, doctype=None, date=None):
    with psycopg2.connect(**settings.TABELLIO_PROCEDURE_DB) as conn:
        with conn.cursor() as cur:
            query = '''SELECT imageid FROM t_document
                            WHERE imageid IS NOT NULL
                              AND sess = %(sess)s'''

            if no:
                query += ' AND no = %(no)s'

            if nodoc:
                query += ' AND nodoc = %(nodoc)s'
            else:
                query += ' AND nodoc IS NULL'

            if anx:
                query += ' AND anx = %(anx)s'
            else:
                query += ' AND anx IS NULL'

            if doctype:
                doctype = settings.TABELLIO_DOCUMENT_TYPES.get(doctype)
                query += ' AND type = %(doctype)s'

            if date:
                query += ' AND date = %(date)s'

            if settings.TABELLIO_FILTERED_TYPES:
                query += ' AND type NOT IN %(filtered_types)s'

            cur.execute(
                query,
                {
                    'sess': sess,
                    'no': no,
                    'nodoc': nodoc,
                    'anx': anx,
                    'doctype': doctype,
                    'date': date,
                    'filtered_types': tuple(settings.TABELLIO_FILTERED_TYPES or []),
                },
            )
            rows = cur.fetchall()
            if len(rows) > 1:
                raise Http404('not unique')
            if rows:
                url = 'https://archives.pfwb.be/%s' % rows[0][0]
                if request.environ.get('QUERY_STRING'):
                    url += '?%s' % request.environ.get('QUERY_STRING')
                return HttpResponseRedirect(url)

    raise Http404()
