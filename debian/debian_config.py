import glob
import itertools
import os

ADMINS = (('admin', 'root@localhost'),)
DEBUG = False
ALLOWED_HOSTS = ['*']

ETC_DIR = '/etc/pfwb-archives-proxy'

with open(os.path.join(ETC_DIR, 'secret')) as fd:
    SECRET_KEY = fd.read()

for filename in itertools.chain(
    [os.path.join(ETC_DIR, 'settings.py')], sorted(glob.glob(os.path.join(ETC_DIR, 'settings.d', '*.py')))
):
    with open(filename) as fd:
        exec(fd.read())
